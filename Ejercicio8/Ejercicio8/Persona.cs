﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio8
{
    class Persona
    {
        protected string nombre;
        protected int edad;
        protected char sexo;
        protected bool asistencia;

        private string[] Nombres_H = new string[5] { "PP", "Emanuel","Ignacio","Lautaro","Daniel"};
        private string[] Nombres_F = new string[5] { "Maria", "Lorena", "Morena", "Clara", "Zoe" };

        private int chico = 0;
        private int chica = 1;

        public Persona()
        {
            int SetSexo = MetodoRandom.NroRandom(0,1);
            if(SetSexo == 0)
            {
                this.nombre = Nombres_H[MetodoRandom.NroRandom(0, 4)];
                this.sexo = 'H';
            }
            else
            {
                this.nombre = Nombres_F[MetodoRandom.NroRandom(0, 4)];
                this.sexo = 'M';
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }
        }
        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                edad = value;
            }
        }
        public char Sexo
        {
            get
            {
                return sexo;
            }
        }
        public bool Asistencia
        {
            get
            {
                return asistencia;
            }
        }

        protected int DarAsistencia()
        {
            Random rdn = new Random();
            int asist = rdn.Next(100);
            return asist;
        }
    }
}
