﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio8
{
    class MetodoRandom
    {
        public static int NroRandom(int minimo, int maximo)
        {
            Random rdn = new Random();
            int nro = rdn.Next(minimo, maximo);
            return nro;
        }
    }
}
