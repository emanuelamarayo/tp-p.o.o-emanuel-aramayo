﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio8
{
    class Aula
    {
        private int id;
        private Profesor profesor;
        private Alumno[] alumnos;
        private string materia;
        private int cant_alumnos = 20;
        private string[] materias = new string[3] { "Matematicas", "Filosofia", "Fisica" };

        public Aula(int id)
        {
            this.id = id;
            this.materia = materias[MetodoRandom.NroRandom(0, 2)];
            alumnos = new Alumno[this.cant_alumnos];
            CrearAlumnos();
            CrearProfesor();
        }

        private void CrearAlumnos()
        {
            for(int i = 0; i < alumnos.Length; i++)
            {
                alumnos[i] = new Alumno();
            }
        }
        private void CrearProfesor()
        {
            profesor = new Profesor();
        }
        private bool AsistenciaAlumnos()
        {
            int cont_asist = 0;

            for(int i = 0; i < alumnos.Length; i++)
            {
                if (alumnos[i].Asistencia)
                {
                    cont_asist++;
                }
            }
            Console.WriteLine("Hay " + cont_asist + " alumnos");

            return cont_asist >= ((int)(alumnos.Length / 2));
        }
        public bool DarClase()
        {
            if (profesor.Asistencia == false)
            {
                Console.WriteLine("No se puede dar clases debido a que el profesor esta ausente");
                return false;
            }
            else if (!profesor.Materia.Equals(materia))
            {
                Console.WriteLine("No se puede dar clases debido a que la materia del profesor y del aula no son la misma");
                return false;
            }
            else if (!AsistenciaAlumnos())
            {
                Console.WriteLine("No se puede dar clases debido a que no hay suficientes alumnos presentes");
                return false;
            }
            Console.WriteLine("Se puede dar clases :(");
            return true;
        }
        public void Notas()
        {
            int chicos_aprobados = 0;
            int chicas_aprobadas = 0;

            for(int i=0;i< alumnos.Length; i++)
            {
                if(alumnos[i].Nota >= 6)
                {
                    if (alumnos[i].Sexo == 'H')
                        chicos_aprobados++;
                    else
                        chicas_aprobadas++;
                }
            }
            Console.WriteLine("Cantidad de chicos aprobados: " + chicos_aprobados);
            Console.WriteLine("");
            Console.WriteLine("Cantidad de chicas aprobadas: " + chicas_aprobadas);
        }
    }
}
