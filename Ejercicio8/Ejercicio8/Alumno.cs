﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio8
{
    class Alumno : Persona
    {
        private int nota;

        public Alumno()
        {
            this.nota = MetodoRandom.NroRandom(0, 10);
            this.edad = MetodoRandom.NroRandom(12, 15);
            DarAsistencia();
        }

        public int Nota
        {
            get
            {
                return nota;
            }
            set
            {
                nota = value;
            }
        }
        new public void DarAsistencia()
        {
            int nro = base.DarAsistencia();
            if (nro < 50)
                asistencia = true;
            else
                asistencia = false;
        }
    }
}
