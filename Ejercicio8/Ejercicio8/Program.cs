﻿using System;

namespace Ejercicio8
{
    class Program
    {
        static void Main(string[] args)
        {
            Aula aula = new Aula(13);

            if (aula.DarClase())
                aula.Notas();
        }
    }
}
