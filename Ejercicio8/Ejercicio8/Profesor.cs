﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio8
{
    class Profesor : Persona
    {
        private string materia;
        private string[] materias = new string[3] { "Matematicas", "Filosofia", "Fisica" };

        public Profesor()
        {
            this.edad = MetodoRandom.NroRandom(25, 50);
            this.materia = materias[MetodoRandom.NroRandom(0, 2)];
            DarAsistencia();
        }

        public string Materia
        {
            get
            {
                return materia;
            }
            set
            {
                materia = value;
            }
        }
        new private void DarAsistencia()
        {
            int nro = MetodoRandom.NroRandom(0,100);
            if (nro < 20)
                asistencia = true;
            else
                asistencia = false;
        }
    }
}
