﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio7
{
    class Raices
    {
        private double a;
        private double b;
        private double c;

        public Raices (double a, double b, double c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        public void ObtenerRaices()
        {
            double raiz1 = (-b + GetDiscriminante()) / 2 * a;
            double raiz2 = (-b - GetDiscriminante()) / 2 * a;
            Console.WriteLine("Raiz 1 = " + raiz1);
            Console.WriteLine("Raiz 2 = " + raiz2);
        }
        public void ObtenerRaiz()
        {
            double raiz = (-b) / (2 * a);
            Console.WriteLine("Raiz = " + raiz);
        }
        public double GetDiscriminante()
        {
            return (b*b) - 4 * a * c;
        }
        public bool TieneRaices()
        {
            bool x = false;
            if (GetDiscriminante() > 0) x = true;
            return x;
        }
        public bool TieneRaiz()
        {
            bool x = false;
            if (GetDiscriminante() == 0) x = true;
            return x;
        }
        public void Calcular()
        {
            if (TieneRaices()) ObtenerRaices();
            else if (TieneRaiz()) ObtenerRaiz();
            else Console.WriteLine("Sin raiz :(");
        }
    }
}
