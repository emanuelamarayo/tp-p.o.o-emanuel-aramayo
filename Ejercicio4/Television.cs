﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio4
{
    class Television : Electrodomestico
    {
        protected int resolucion = 20;
        protected bool sintonizadorTDT = false;

        public Television()
        {

        }
        public Television(double precio_base, int peso) : base (precio_base, peso)
        {

        }
        public Television(int resolucion, bool sintonizadorTDT, double precio_base, string color, char consumo_energetico, int peso) : base (precio_base, color, consumo_energetico, peso)
        {
            this.resolucion = resolucion;
            this.sintonizadorTDT = sintonizadorTDT;
        }
        public int Resolucion
        {
            get
            {
                return resolucion;
            }
        }
        public bool SintonizadorTDT
        {
            get
            {
                return sintonizadorTDT;
            }
        }
        new public void PrecioFinal()
        {
            base.PrecioFinal();

            if (resolucion > 40)
            {
                precio_base = precio_base * 1.3;
            }
            if (sintonizadorTDT)
            {
                precio_base += 50;
            }

        }
    }
}
