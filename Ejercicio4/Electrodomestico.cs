﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio4
{
    class Electrodomestico
    {
        protected double precio_base = 100;
        protected string color = "blanco";
        protected char consumo_energetico = 'F';
        protected int peso = 5;

        public Electrodomestico()
        {
            ComprobarConsumoEnergetico(consumo_energetico);
            ComprobarColor(color);
        }
        public Electrodomestico(double precio_base, int peso)
        {
            this.precio_base = precio_base;
            this.peso = peso;
            ComprobarConsumoEnergetico(consumo_energetico);
            ComprobarColor(color);
        }
        public Electrodomestico(double precio_base, string color, char consumo_energetico, int peso)
        {
            this.precio_base = precio_base;
            this.color = color;
            this.consumo_energetico = consumo_energetico;
            this.peso = peso;
            ComprobarConsumoEnergetico(consumo_energetico);
            ComprobarColor(color);
        }
        public double Precio_base
        {
            get
            {
                return precio_base;
            }
            set
            {
                precio_base = value;
            }
        }
        public string Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }
        public char Consumo_energetico
        {
            get
            {
                return consumo_energetico;
            }
            set
            {
                consumo_energetico = value;
            }
        }
        public int Peso
        {
            get
            {
                return peso;
            }
            set
            {
                peso = value;
            }
        }
        public void ComprobarConsumoEnergetico(char letra)
        {
            string letras = "abcdefABCDEF";

            if (letras.Contains(letra))
            {
                consumo_energetico = letra;
            }
            else consumo_energetico = 'f';
        }
        public void ComprobarColor(string color)
        {
            string blanco = "blanco";
            string Blanco = "BLANCO";
            string negro = "negro";
            string Negro = "NEGRO";
            string rojo = "rojo";
            string Rojo = "ROJO";
            string azul = "azul";
            string Azul = "AZUL";
            string gris = "gris";
            string Gris = "GRIS";

            if (blanco.Contains(color) || Blanco.Contains(color))
            {
                this.color = color;
            }
            if (negro.Contains(color) || Negro.Contains(color))
            {
                this.color = color;
            }
            if (rojo.Contains(color) || Rojo.Contains(color))
            {
                this.color = color;
            }
            if (azul.Contains(color) || Azul.Contains(color))
            {
                this.color = color;
            }
            if (gris.Contains(color) || Gris.Contains(color))
            {
                this.color = color;
            }
            else this.color = blanco;
        }
        public void PrecioFinal()
        {
            string a = "aA";
            string b = "bB";
            string c = "cC";
            string d = "dD";
            string e = "eE";
            string f = "fF";

            if (a.Contains(consumo_energetico))
            {
                precio_base += 100;
            }
            if (b.Contains(consumo_energetico))
            {
                precio_base += 80;
            }
            if (c.Contains(consumo_energetico))
            {
                precio_base += 60;
            }
            if (d.Contains(consumo_energetico))
            {
                precio_base += 50;
            }
            if (e.Contains(consumo_energetico))
            {
                precio_base += 30;
            }
            if (f.Contains(consumo_energetico))
            {
                precio_base += 10;
            }
            if (peso <= 19)
            {
                precio_base += 10;
            }
            if (peso >= 20 && peso <= 49)
            {
                precio_base += 50;
            }
            if (peso >= 50 && peso <= 79)
            {
                precio_base += 80;
            }
            if (peso >= 80)
            {
                precio_base += 100;
            }
        }
    }
}
