﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Ejercicio4
{
    class Lavadora : Electrodomestico
    {
        protected int carga = 5;

        public Lavadora()
        {

        }
        public Lavadora(double precio_base, int peso)
        {

            this.precio_base = precio_base;
            this.peso = peso;
        }
        public Lavadora(int carga, double precio_base, string color, char consumo_energetico, int peso) : base (precio_base, color, consumo_energetico, peso)
        {
            this.precio_base = precio_base;
            this.color = color;
            this.consumo_energetico = consumo_energetico;
            this.peso = peso;
            this.carga = carga;

        }
        public int Carga
        {
            get
            {
                return carga;
            }
        }
        new public void PrecioFinal()
        {
            base.PrecioFinal();
            if(carga > 30)
            {
                precio_base += 50;
            }
        }
    }
}
