﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio4
{
    class Ejecutable
    {
        public void Ejecutar()
        {
            Electrodomestico[] electrodomesticos = new Electrodomestico[10];

            electrodomesticos[0] = new Electrodomestico();
            electrodomesticos[1] = new Electrodomestico(500, 100);
            electrodomesticos[2] = new Electrodomestico(200, "negro", 'b', 50);

            electrodomesticos[3] = new Lavadora();
            electrodomesticos[4] = new Lavadora(200,30);
            electrodomesticos[5] = new Lavadora(10,40,"rojo",'a',60);

            electrodomesticos[6] = new Television();
            electrodomesticos[7] = new Television(70,90);
            electrodomesticos[8] = new Television(20,false,35,"azul",'d',100);
            electrodomesticos[9] = new Television(50,true,40,"Violeta",'T',80);

            for(int i = 0; i < 10; i++)
            {
                electrodomesticos[i].PrecioFinal();
            }

            double precio_electrodomesticos = 0;
            double precio_lavadora = 0;
            double precio_television = 0;
            double precio_total = 0;

            precio_electrodomesticos = electrodomesticos[0].Precio_base + electrodomesticos[1].Precio_base + electrodomesticos[2].Precio_base;

            precio_lavadora = electrodomesticos[3].Precio_base + electrodomesticos[4].Precio_base + electrodomesticos[5].Precio_base;

            precio_television = electrodomesticos[6].Precio_base + electrodomesticos[7].Precio_base + electrodomesticos[8].Precio_base + electrodomesticos[9].Precio_base;

            precio_total = precio_electrodomesticos + precio_lavadora + precio_television;

            Console.WriteLine("La suma de los electrodomesticos es de: " + precio_electrodomesticos);
            Console.WriteLine("La suma de las lavadoras es de: " + precio_lavadora);
            Console.WriteLine("La suma de las televisiones es de: " + precio_television);
            Console.WriteLine("La suma total es de: " + precio_total);


        }
    }
}
