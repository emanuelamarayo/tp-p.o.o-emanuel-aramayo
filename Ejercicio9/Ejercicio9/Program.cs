﻿using System;

namespace Ejercicio9
{
    class Program
    {
        static void Main(string[] args)
        {
            Pelicula pelicula = new Pelicula("Febrero :(", 10, 18, "Emanuel Aramayo");

            Console.WriteLine("Ingresar Filas");
            int filas = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingresar Columnas");
            int columnas = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingresar Precio");
            double precio = double.Parse(Console.ReadLine());

            Cine cine = new Cine(filas, columnas, precio, pelicula);

            Console.WriteLine("Numero de Espectadores :D");
            int numEspectadores = int.Parse(Console.ReadLine());

            Espectador e;
            int fila;
            char letra;

            static string generarNombre()
            {
                Random rdn = new Random();
                string[] nombres = new string[5];
                nombres[0] = "PP";
                nombres[1] = "PP2";
                nombres[2] = "PP3";
                nombres[3] = "PP4";
                nombres[4] = "PP5";

                int nro = rdn.Next(0, 4);

                return nombres[nro];
            }
            static int generarEdad()
            {
                Random rdn = new Random();

                return rdn.Next(6, 99);
            }
            static double generarDinero()
            {
                Random rdn = new Random();

                return rdn.NextDouble() * (500.00 - 100.00) + 100.00;
            }
            static int numeroAleatorio(int minimo, int maximo)
            {
                Random rdn = new Random();
                return rdn.Next(minimo, maximo);
            }
            
            for(int i = 0; i < numEspectadores; i++)
            {
                e = new Espectador(generarNombre(), generarEdad(), generarDinero());
                Console.WriteLine(e.Nombre);
                Console.WriteLine(e.Edad);
                Console.WriteLine(e.Dinero);

                do
                {
                    fila = numeroAleatorio(0, cine.getFilas() - 1);
                    letra = (char)numeroAleatorio('A', 'A' + (cine.getColumnas() - 1));
                } while (cine.HaySitioButaca(fila, letra));
                if (cine.SePuedeSentar(e))
                {
                    e.Pagar(cine.Precio);
                    cine.Sentar(fila, letra, e);
                }
            }
            cine.mostrar();
        }
    }
}
