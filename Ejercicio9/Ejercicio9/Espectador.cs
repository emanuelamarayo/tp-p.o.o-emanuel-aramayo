﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio9
{
    class Espectador
    {
        private string nombre;
        private int edad;
        private double dinero;

        public Espectador(string nombre, int edad, double dinero)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.dinero = dinero;
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                nombre = value;
            }
        }
        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                edad = value;
            }
        }
        public double Dinero
        {
            get
            {
                return dinero;
            }
            set
            {
                dinero = value;
            }
        }
        public void Pagar(double precio)
        {
            dinero -= precio;
        }
        public Boolean TieneEdad(int edadMinima)
        {
            return edad >= edadMinima;
        }
        public bool TieneDinero(double precioEntrada)
        {
            return dinero >= precioEntrada;
        }
    }
}
