﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio9
{
    class Pelicula
    {

        private string titulo;
        private int duracion;
        private int edadMinima;
        private string director;

        public Pelicula(string titulo, int duracion, int edadMinima, string director)
        {
            this.titulo = titulo;
            this.duracion = duracion;
            this.edadMinima = edadMinima;
            this.director = director;
        }

        public string Titulo
        {
            get
            {
                return titulo;
            }
            set
            {
                titulo = value;
            }
        }
        public int Duracion
        {
            get
            {
                return duracion;
            }
            set
            {
                duracion = value;
            }
        }
        public int EdadMinima
        {
            get
            {
                return edadMinima;
            }
            set
            {
                edadMinima = value;
            }
        }
        public string Director
        {
            get
            {
                return director;
            }
            set
            {
                director = value;
            }
        }
    }
}
