﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio9
{
    class Cine
    {
        private Asiento[,] asientos;
        private double precio;
        private Pelicula pelicula;

        public Cine(int filas, int columnas, double precio, Pelicula pelicula)
        {
            asientos = new Asiento[filas, columnas];
            this.precio = precio;
            this.pelicula = pelicula;
            RellenarButacas();
        }
        public Asiento[,] Asientos
        {
            get
            {
                return asientos;
            }
        }
        public double Precio
        {
            get
            {
                return precio;
            }
            set
            {
                precio = value;
            }
        }
        private void RellenarButacas()
        {
            int fila = asientos.GetLength(0);
            for(int i = 0; i < asientos.GetLength(0); i++)
            {
                for (int j = 0; j < asientos.GetLength(1); j++)
                {
                    asientos[i, j] = new Asiento((char)('A' + j), fila);
                }
                fila--;
            }
        }
        public bool HaySitio()
        {
            for(int i = 0; i < asientos.GetLength(0); i++)
            {
                for(int j = 0; j < asientos.GetLength(1); j++)
                {
                    if (!asientos[i, j].Ocupado())
                        return true;
                }
            }
            return false;
        }
        public bool HaySitioButaca(int fila, char letra)
        {
            return getAsiento(fila, letra).Ocupado();
        }
        public bool SePuedeSentar(Espectador e)
        {
            return e.TieneDinero(precio) && e.TieneEdad(pelicula.EdadMinima);
        }
        public void Sentar(int fila, char letra, Espectador e)
        {
            getAsiento(fila, letra).setEspectador(e);
        }
        public Asiento getAsiento(int fila, char letra)
        {
            return asientos[asientos.GetLength(0) - fila - 1, letra - 'A'];
        }
        public int getFilas()
        {
            return asientos.GetLength(0);
        }
        public int getColumnas()
        {
            return asientos.GetLength(1);
        }
        public void mostrar()
        {
            Console.WriteLine("Info del Cine");
            Console.WriteLine("Pelicula reproducida: " + pelicula);
            Console.WriteLine("Precio de entrada: " + precio);
            Console.WriteLine("");
            for(int i=0; i< asientos.GetLength(0); i++)
            {
                for(int j = 0; j < asientos.GetLength(1); j++)
                {
                    Console.WriteLine($"fila : {i} Letra : {j} Ocupados {asientos[i, j].Ocupado()}");
                }
                Console.WriteLine("");
            }
        }
    }
}
