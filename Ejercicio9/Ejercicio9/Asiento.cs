﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio9
{
    class Asiento
    {

        private char letra;
        private int fila;
        private Espectador espectador;

        public Asiento(char letra, int fila)
        {
            this.letra = letra;
            this.fila = fila;
            this.espectador = null;
        }
        public char Letra
        {
            get
            {
                return letra;
            }
            set
            {
                letra = value;
            }
        }
        public int Fila
        {
            get
            {
                return fila;
            }
            set
            {
                fila = value;
            }
        }
        public Espectador Espectador
        {
            get
            {
                return espectador;
            }
        }
        public bool Ocupado()
        {
            return espectador != null;
        }
        public void setEspectador(Espectador espectador)
        {
            this.espectador = espectador;
        }
    }
}
