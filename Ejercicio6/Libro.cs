﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio6
{
    class Libro
    {
        private int isbn;
        private string titulo;
        private string autor;
        private int nro_pags;

        public Libro(int isbn, string titulo, string autor, int nro_pags)
        {
            this.isbn = isbn;
            this.titulo = titulo;
            this.autor = autor;
            this.nro_pags = nro_pags;
        }
        public int Isbn
        {
            get
            {
                return isbn;
            }
            set
            {
                isbn = value;
            }
        }
        public string Titulo
        {
            get
            {
                return titulo;
            }
            set
            {
                titulo = value;
            }
        }
        public string Autor
        {
            get
            {
                return autor;
            }
            set
            {
                autor = value;
            }
        }
        public int Nro_pags
        {
            get
            {
                return nro_pags;
            }
            set
            {
                nro_pags = value;
            }
        }
        override public string ToString()
        {
            string msj = "El libro " + titulo + " con ISBN " + isbn + " creado por el autor " + autor + " tiene " + nro_pags + " páginas.";

            return msj;
        }
    }
}
