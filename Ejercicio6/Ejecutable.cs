﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio6
{
    class Ejecutable
    {
        public void Ejecutar()
        {
            Libro libro1 = new Libro(12345, "Harry Potter", "J.K. Rowling", 500);
            Libro libro2 = new Libro(67891, "Percy Jackson", "Rick Riordan", 1000);

            Console.WriteLine(libro1.ToString());
            Console.WriteLine(libro2.ToString());

            if (libro1.Nro_pags > libro2.Nro_pags)
            {
                Console.WriteLine("El libro " + libro1.Titulo + " tiene más páginas");
            }

            else Console.WriteLine("El libro " + libro2.Titulo + " Tiene más páginas");
        }
    }
}
