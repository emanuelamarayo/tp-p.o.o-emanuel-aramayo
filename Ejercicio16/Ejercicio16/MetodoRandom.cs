﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio16
{
    class MetodoRandom
    {
        public static int Rdn(int minimo, int maximo)
        {
            Random rdn = new Random();
            int nro = rdn.Next(minimo, maximo);
            return nro;
        }
    }
}
