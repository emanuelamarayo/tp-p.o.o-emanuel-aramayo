﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio16
{
    abstract class Baraja<T>
    {
        protected Carta<T>[] cartas;
        protected int posSiguienteCarta;
        protected int numCartas;
        protected int cartasPorPalo;

        public Baraja()
        {
            this.posSiguienteCarta = 0;
        }
        public abstract void crearBaraja();

        public void barajar()
        {
            int posAleatoria = 0;

            Carta<T> carta;

            for (int i = 0; i < cartas.Length; i++)
            {
                posAleatoria = MetodoRandom.Rdn(0, numCartas - 1);
                carta = cartas[i];
                cartas[i] = cartas[posAleatoria];
                cartas[posAleatoria] = carta;
            }
            this.posSiguienteCarta = 0;
        }
        public Carta<T> SiguienteCarta()
        {
            Carta<T> carta = null;

            if (posSiguienteCarta == numCartas)
                Console.WriteLine("No quedan cartas");
            else carta = cartas[posSiguienteCarta++];
            return carta;
        }
        public Carta<T>[] darCartas(int numCartas)
        {
            if (numCartas > numCartas)
                Console.WriteLine("No se pueden dar mas cartas");
            else if (CartasDisponibles() < numCartas)
                Console.WriteLine("No alcanzan las cartas");
            else
            {
                Carta<T>[] DarCartas = new Carta<T>[numCartas];

                for (int i = 0; i < DarCartas.Length; i++)
                {
                    DarCartas[i] = SiguienteCarta();
                }
                return DarCartas;
            }
            return null;
        }
        public int CartasDisponibles()
        {
            return numCartas - posSiguienteCarta;
        }
        public void CartasMonton()
        {
            if (CartasDisponibles() == numCartas)
                Console.WriteLine("Todavia no se saco una carta");
            else
            {
                for (int i = 0; i < posSiguienteCarta; i++)
                {
                    Console.WriteLine($"{cartas[i].Numero} {cartas[i].Palo}");
                }
            }
        }
        public void MostrarBaraja()
        {
            if (CartasDisponibles() == 0)
                Console.WriteLine("No quedan cartas");
            else
            {
                for (int i = 0; i < posSiguienteCarta; i++)
                {
                    Console.WriteLine(cartas[i].Numero + " de " + cartas[i].Palo);
                }
            }
        }
    }
}
