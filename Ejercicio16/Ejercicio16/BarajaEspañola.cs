﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio16
{
    class BarajaEspañola : Baraja<PalosBarajaEspañola>
    {
        private bool incluye_8_9;

        public BarajaEspañola(bool incluye_8_9) : base()
        {
            this.incluye_8_9 = incluye_8_9;

            if (incluye_8_9)
            {
                numCartas = 48;
                cartasPorPalo = 12;
            }
            else
            {
                numCartas = 40;
                cartasPorPalo = 10;
            }

            crearBaraja();
            base.barajar();
        }
        public override void crearBaraja()
        {
            this.cartas = (Carta<PalosBarajaEspañola>[])new Carta<PalosBarajaEspañola>[numCartas];

            PalosBarajaEspañola[] palos = new PalosBarajaEspañola[4];
            palos[0] = PalosBarajaEspañola.BASTOS;
            palos[1] = PalosBarajaEspañola.COPAS;
            palos[2] = PalosBarajaEspañola.ESPADAS;
            palos[3] = PalosBarajaEspañola.OROS;

            for(int i = 0; i < palos.Length; i++)
            {
                for(int j = 0; j < cartasPorPalo; j++)
                {
                    if (incluye_8_9)
                        cartas[((i * cartasPorPalo) + j)] = new Carta<PalosBarajaEspañola>(j + 1, palos[i]);
                    else
                        cartas[((i * cartasPorPalo) + j)] = new Carta<PalosBarajaEspañola>(j + 3, palos[i]);
                }
            }
        }
    }
}
