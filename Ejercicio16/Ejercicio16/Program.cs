﻿using System;

namespace Ejercicio16
{
    public enum PalosBarajaEspañola
    {
        OROS,
        COPAS,
        ESPADAS,
        BASTOS
    }
    public enum PalosBarajaFrancesa
    {
        DIAMANTES,
        PICAS,
        TREBOLES,
        CORAZONES
    }
    class Program
    {
        static void Main(string[] args)
        {
            BarajaFrancesa b = new BarajaFrancesa();

            Console.WriteLine("Hay " + b.CartasDisponibles() + " cartas disponibles");

            b.SiguienteCarta();

            b.darCartas(5);

            Console.WriteLine("Hay " + b.CartasDisponibles() + " cartas disponibles");

            Console.WriteLine("Cartas sacadas de momento:");

            b.CartasMonton();

            b.barajar();

            Carta<PalosBarajaFrancesa>[] c = b.darCartas(5);
            Console.WriteLine("Cartas sacadas despues de barajar");

            for(int i = 0; i < c.Length; i++)
            {
                Console.WriteLine($"{c[i].Numero} {c[i].Palo}");
            }
        }
    }
}
