﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio16
{
    class Carta <T>
    {
        private int numero;
        private T palo;

        public int Numero
        {
            get
            {
                return numero;
            }
        }
        public T Palo
        {
            get
            {
                return palo;
            }
        }
        public Carta(int numero,T palo)
        {
            this.numero = numero;
            this.palo = palo;
        }
    }
}
