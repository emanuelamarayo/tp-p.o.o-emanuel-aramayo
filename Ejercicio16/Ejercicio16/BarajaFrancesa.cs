﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio16
{
    class BarajaFrancesa : Baraja<PalosBarajaFrancesa>
    {
        public BarajaFrancesa() : base()
        {
            this.numCartas = 52;
            this.cartasPorPalo = 11;

            crearBaraja();
            base.barajar();
        }
        public override void crearBaraja()
        {
            this.cartas = (Carta<PalosBarajaFrancesa>[])new Carta<PalosBarajaFrancesa>[numCartas];
            PalosBarajaFrancesa[] palos = new PalosBarajaFrancesa[4];
            palos[0] = PalosBarajaFrancesa.CORAZONES;
            palos[1] = PalosBarajaFrancesa.DIAMANTES;
            palos[2] = PalosBarajaFrancesa.PICAS;
            palos[3] = PalosBarajaFrancesa.TREBOLES;

            for(int i = 0; i < palos.Length; i++)
            {
                for(int j = 0; j < cartasPorPalo; j++)
                {
                    cartas[((i * cartasPorPalo) + j)] = new Carta<PalosBarajaFrancesa>(j + 1, palos[i]);
                }
            }
        }
        public bool CartaRoja(Carta<PalosBarajaFrancesa> c)
        {
            return c.Palo == PalosBarajaFrancesa.CORAZONES || c.Palo == PalosBarajaFrancesa.DIAMANTES;
        }
        public bool CartaNegra(Carta<PalosBarajaFrancesa> c)
        {
            return c.Palo == PalosBarajaFrancesa.TREBOLES || c.Palo == PalosBarajaFrancesa.PICAS;
        }
    }
}
