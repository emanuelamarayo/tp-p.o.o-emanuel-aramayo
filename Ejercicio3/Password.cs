﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.Text;

namespace Ejercicio3
{
    class Password
    {
        private int longitud = 8;
        private string contrasena;

        public Password()
        {

        }
        public Password(int longitud)
        {
            this.longitud = longitud;
            GenerarContrasena();
        }
        public bool EsFuerte()
        {
            string mayusculas = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
            int contmayus = 0;

            string minusculas = "abcdefghijklmnñopqrstuvwxyz";
            int contminus = 0;

            string nums = "0123456789";
            int contnums = 0;

            for(int i = 0;i < mayusculas.Length; i++)
            {
                if (contrasena.Contains(mayusculas[i]) == true)
                {
                    contmayus++;
                }
            }
            for (int i = 0; i < minusculas.Length; i++)
            {
                if (contrasena.Contains(minusculas[i]) == true)
                {
                    contminus++;
                }
            }
            for (int i = 0; i < nums.Length; i++)
            {
                if (contrasena.Contains(nums[i]) == true)
                {
                    contnums++;
                }
            }

            if (contmayus > 2 && contminus > 1 && contnums > 5)
            {
                return true;
            }
            else return false;
        }
        public void GenerarContrasena()
        {
            string alfabeto = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789";
            int long_text = alfabeto.Length;
            char caracter;
            string contrasenaRandom = "";
            Random contrasena = new Random();

            for (int i = 0; i < longitud; i++)
            {
                caracter = alfabeto[contrasena.Next(long_text)];
                contrasenaRandom += caracter.ToString();
            }
            this.contrasena = contrasenaRandom;  
        }
        public string Contrasena
        {
            get
            {
                return contrasena;
            }
        }
        public int Longitud
        {
            get
            {
                return longitud;
            }
            set
            {
                longitud = value;
            }
        }
    }
}
