﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio3
{
    class Ejecutable
    {
        public void Ejecutar()
        {
            Console.WriteLine("Indique longitud del array Passwords");
            int longitud_array = int.Parse(Console.ReadLine());

            Password[] Passwords = new Password[longitud_array];
            bool[] booleanos = new bool[longitud_array];

            for(int i=0; i < longitud_array; i++)
            {
                Console.WriteLine("Indique la longitud de la contraseña");
                int longitud_contrasena = int.Parse(Console.ReadLine());
                Passwords[i] = new Password(longitud_contrasena);
                booleanos[i] = Passwords[i].EsFuerte();
                Console.WriteLine("Su contraseña es " + Passwords[i].Contrasena + " Es fuerte?: " + Passwords[i].EsFuerte());
            }
        } 
    }
}
