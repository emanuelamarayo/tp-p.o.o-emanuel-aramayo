﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio13
{
    class NoPerecedero : Producto
    {
        private string tipo;

        public NoPerecedero(string tipo, string nombre, double precio) : base(nombre, precio)
        {
            this.tipo = tipo;
        }
        public string Tipo
        {
            get
            {
                return tipo;
            }
            set
            {
                tipo = value;
            }
        }
    }
}
