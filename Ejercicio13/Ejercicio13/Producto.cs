﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio13
{
    class Producto
    {
        private string nombre;
        private double precio;

        public Producto(string nombre, double precio)
        {
            this.nombre = nombre;
            this.precio = precio;
        }
        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                nombre = value;
            }
        }
        public double Precio
        {
            get
            {
                return precio;
            }
            set
            {
                precio = value;
            }
        }
        public double Calcular(int cantidad)
        {
            return precio * cantidad;
        }
    }
}
