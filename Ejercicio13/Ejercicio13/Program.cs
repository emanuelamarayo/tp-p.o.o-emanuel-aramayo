﻿using System;

namespace Ejercicio13
{
    class Program
    {
        static void Main(string[] args)
        {
            Producto[] productos = new Producto[3];

            productos[0] = new Producto("producto 1", 25);
            productos[1] = new Perecedero(2, "producto 2", 16);
            productos[2] = new NoPerecedero("Tipo 1","producto 3", 10);

            double total = 0;
            for(int i = 0; i < productos.Length; i++)
            {
                total += productos[i].Calcular(5);
            }
            Console.WriteLine("El total es: " + total);
        }
    }
}
