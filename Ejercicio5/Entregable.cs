﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio5
{
    interface Entregable
    {
        bool Entregar();

        bool Devolver();

        bool IsEntregado();

        int CompareTo(Object a);
    }
}
