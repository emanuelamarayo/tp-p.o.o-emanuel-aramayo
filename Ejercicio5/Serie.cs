﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio5
{
    class Serie : Entregable
    {
        private string titulo;
        private int nro_temporadas = 3;
        private bool entregado = false;
        private string genero;
        private string creador;

        public Serie()
        {

        }
        public Serie(string titulo, string creador)
        {
            this.titulo = titulo;
            this.creador = creador;
        }
        public Serie(string titulo, int nro_temporadas, string genero, string creador)
        {
            this.titulo = titulo;
            this.nro_temporadas = nro_temporadas;
            this.genero = genero;
            this.creador = creador;
        }
        public string Titulo
        {
            get
            {
                return titulo;
            }
            set
            {
                titulo = value;
            }
        }
        public int Nro_temporadas
        {
            get
            {
                return nro_temporadas;
            }
            set
            {
                nro_temporadas = value;
            }
        }
        public string Genero
        {
            get
            {
                return genero;
            }
            set
            {
                genero = value;
            }
        }
        public string Creador
        {
            get
            {
                return creador;
            }
            set
            {
                creador = value;
            }
        }
        public bool Entregar()
        {
            entregado = true;
            return entregado;
        }
        public bool Devolver()
        {
            entregado = false;
            return entregado;
        }
        public bool IsEntregado()
        {
            return entregado;
        }
        public int CompareTo(Object a)
        {
            Serie serie = (Serie)a;

            int nro = 0;

            if(this.nro_temporadas < serie.Nro_temporadas)
            {
                nro = 1;
                return nro;
            }
            if (this.nro_temporadas > serie.Nro_temporadas)
            {
                nro = 2;
                return nro;
            }
            else return nro;
        }
    }
}
