﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio5
{
    class Videojuego : Entregable
    {
        private string titulo;
        private int hs_estimadas = 10;
        private bool entregado = false;
        private string genero;
        private string compania;

        public Videojuego()
        {

        }
        public Videojuego(string titulo, int hs_estimadas)
        {
            this.titulo = titulo;
            this.hs_estimadas = hs_estimadas;
        }
        public Videojuego(string titulo, int hs_estimadas, string genero, string compania)
        {
            this.titulo = titulo;
            this.hs_estimadas = hs_estimadas;
            this.genero = genero;
            this.compania = compania;
        }
        public string Titulo
        {
            get
            {
                return titulo;
            }
            set
            {
                titulo = value;
            }
        }
        public int Hs_estimadas
        {
            get
            {
                return hs_estimadas;
            }
            set
            {
                hs_estimadas = value;
            }
        }
        public string Genero
        {
            get
            {
                return genero;
            }
            set
            {
                genero = value;
            }
        }
        public string Compania
        {
            get
            {
                return compania;
            }
            set
            {
                compania = value;
            }
        }
        public bool Entregar()
        {
            entregado = true;
            return entregado;
        }
        public bool Devolver()
        {
            entregado = false;
            return entregado;
        }
        public bool IsEntregado()
        {
            return entregado;
        }
        public int CompareTo(Object a)
        {
            Videojuego videojuego = (Videojuego)a;

            int nro = 0;

            if (this.hs_estimadas < videojuego.Hs_estimadas)
            {
                nro = 1;
                return nro;
            }
            if (this.hs_estimadas > videojuego.Hs_estimadas)
            {
                nro = 2;
                return nro;
            }
            else return nro;
        }
    }
}
