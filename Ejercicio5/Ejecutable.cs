﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio5
{
    class Ejecutable
    {
        public void Ejecutar()
        {
            Serie[] series = new Serie[5];

            Videojuego[] videojuegos = new Videojuego[5];

            series[0] = new Serie();
            series[1] = new Serie("Serie 1", "pp");
            series[2] = new Serie("Serie 2","Tomi");
            series[3] = new Serie("Serie 3", 2, "Terror", "Roy");
            series[4] = new Serie("Serie 4", 5, "Comedia", "Aron");

            videojuegos[0] = new Videojuego();
            videojuegos[1] = new Videojuego("Juego 1", 5);
            videojuegos[2] = new Videojuego("Juego 2", 15);
            videojuegos[3] = new Videojuego("Juego 3", 20, "Accion", "Ubisoft");
            videojuegos[4] = new Videojuego("Juego 4", 100, "Moba","Riot");

            series[1].Entregar();
            series[2].Entregar();

            videojuegos[1].Entregar();
            videojuegos[2].Entregar();

            int contJuegos = 0;
            int contSeries = 0;

            for(int i=0;i < 5; i++)
            {
                if (videojuegos[i].IsEntregado())
                {
                    contJuegos++;
                }
            }
            for(int i=0;i < 5; i++)
            {
                if (series[i].IsEntregado())
                {
                    contSeries++;
                }
            }

            Console.WriteLine("La cantidad de videojuegos entregados es de " + contJuegos);
            Console.WriteLine("La cantidad de series entregadas es de " + contSeries);
            Console.WriteLine("");

            Videojuego aux = new Videojuego();
            Serie aux2 = new Serie();

            for (int i=0;i < 5; i++)
            {
                for(int j=1;j < 5; j++)
                {
                    if(videojuegos[i].CompareTo(videojuegos[j]) == 1)
                    {
                        aux = videojuegos[i];
                        videojuegos[i] = videojuegos[j];
                        videojuegos[j] = aux;
                    }
                }
            }
            for (int i = 0; i < 5; i++)
            {
                for (int j = 1; j < 5; j++)
                {
                    if (series[i].CompareTo(series[j]) == 1)
                    {
                        aux2 = series[i];
                        series[i] = series[j];
                        series[j] = aux2;
                    }
                }
            }

            Console.WriteLine("El videojuego con mas horas estimadas es:" + videojuegos[0].Titulo);
            Console.WriteLine("Su información:");
            Console.WriteLine("Horas estimadas:" + videojuegos[0].Hs_estimadas);
            Console.WriteLine("Genero: " + videojuegos[0].Genero);
            Console.WriteLine("Compania: " + videojuegos[0].Compania);

            Console.WriteLine("");

            Console.WriteLine("La serie con mas temporadas es:" + series[0].Titulo);
            Console.WriteLine("Su información:");
            Console.WriteLine("Temporadas: " + series[0].Nro_temporadas);
            Console.WriteLine("Genero: " + series[0].Genero);
            Console.WriteLine("Creador: " + series[0].Creador);

        }
    }
}
