﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio14
{
    class Bebida
    {
        private static int idActual = 1;

        private int id;
        private double cantidad;
        private double precio;
        private string marca;

        public Bebida(double cantidad, double precio, string marca)
        {
            this.id = idActual++;
            this.cantidad = cantidad;
            this.precio = precio;
            this.marca = marca;
        }
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        public double Cantidad
        {
            get
            {
                return cantidad;
            }
            set
            {
                cantidad = value;
            }
        }
        public double Precio
        {
            get
            {
                return precio;
            }
            set
            {
                precio = value;
            }
        }
        public string Marca
        {
            get
            {
                return marca;
            }
            set
            {
                marca = value;
            }
        }
    }
}
