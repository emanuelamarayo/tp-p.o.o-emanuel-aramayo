﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio14
{
    class BebidaAzucarada : Bebida
    {
        private double porcentajeAzucar;
        private bool promocion;

        public BebidaAzucarada(double porcentajeAzucar, bool promocion, double cantidad, double precio, string marca) : base(cantidad, precio, marca)
        {
            this.porcentajeAzucar = porcentajeAzucar;
            this.promocion = promocion;
        }
        public double PorcentajeAzucar
        {
            get
            {
                return porcentajeAzucar;
            }
            set
            {
                porcentajeAzucar = value;
            }
        }
        public bool Promocion
        {
            get
            {
                return promocion;
            }
            set
            {
                promocion = value;
            }
        }
        public double GetPrecio()
        {
            if (promocion)
                return base.Precio * 0.9;
            else
                return base.Precio;
        }
    }
}
