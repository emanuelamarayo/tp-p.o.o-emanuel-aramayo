﻿using System;

namespace Ejercicio14
{
    class Program
    {
        static void Main(string[] args)
        {
            Almacen a = new Almacen();

            Bebida b;

            for(int i = 0; i < 10; i++)
            {
                switch (i % 2)
                {
                    case 0:
                        b = new AguaMineral("Aramayo", 1.5, 5, "Dia");
                        a.AgregarBebida(b);
                        break;
                    case 1:
                        b = new BebidaAzucarada(0.2, true, 1.5, 10, "Manaos");
                        a.AgregarBebida(b);
                        break;
                }
            }

            a.MostrarBebidas();

            Console.WriteLine("Precio de todas las bebidas: " + a.CalcularPrecioBebidas());

            a.EliminarBebida(4);

            a.MostrarBebidas();

            Console.WriteLine("Precio de todas las bebidas: " + a.CalcularPrecioBebidas());

            Console.WriteLine("Precio de todas las bebidas marca Dia: " + a.CalcularPrecioBebidas("Dia"));

            Console.WriteLine("Calcular el precio de la columna 0: " + a.CalcularPrecioBebidas(0));
        }
    }
}
