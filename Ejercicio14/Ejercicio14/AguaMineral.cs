﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio14
{
    class AguaMineral : Bebida
    {
        private string manantial;
        
        public AguaMineral(string manantial, double cantidad, double precio, string marca) : base(cantidad, precio, marca)
        {
            this.manantial = manantial;
        }
        public string Manantial
        {
            get
            {
                return manantial;
            }
            set
            {
                manantial = value;
            }
        }
    }
}
