﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio1
{
    class Cuenta
    {
        private string titular;
        private double cantidad;

        public Cuenta(string titular)
        {
            this.titular = titular;
        }
        public Cuenta(string titular, double cantidad)
        {
            this.titular = titular;
            this.cantidad = cantidad;
        }
        public string Titular
        {
            get
            {
                return titular;
            }
            set
            {
                titular = value;
            }
        }
        public double Cantidad
        {
            get
            {
                return cantidad;
            }
            set
            {
                cantidad = value;
            }
        }
        public void Ingresar(double cantidad)
        {
            if (cantidad > 0)
            {
                this.cantidad += cantidad;
            }
            else
            {
                return;
            }
        }
        public void Retirar(double cantidad)
        {
            if (cantidad > this.cantidad)
            {
                Console.WriteLine("No puedes retirar más de lo que tienes en la cuenta.");
            }
            else
            {
                this.cantidad -= cantidad;
            }
        }
    }
}
