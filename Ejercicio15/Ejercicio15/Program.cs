﻿using System;

namespace Ejercicio15
{
    class Program
    {
        static void Main(string[] args)
        {
            Agenda a = new Agenda(4);
            string nombre;
            int telefono;

            bool salir = false;

            Contacto c;

            while (!salir)
            {
                Console.WriteLine("");
                Console.WriteLine("Agenda Telefonica :D");
                Console.WriteLine("");
                Console.WriteLine("1. Añadir contacto");
                Console.WriteLine("2. Lista de contactos");
                Console.WriteLine("3. Buscar contacto");
                Console.WriteLine("4. Existe el contacto?");
                Console.WriteLine("5. Eliminar contacto :(");
                Console.WriteLine("6. Contactos disponibles");
                Console.WriteLine("7. Agenda llena?");
                Console.WriteLine("8. Salir");

                int aux = int.Parse(Console.ReadLine());

                switch (aux)
                {
                    case 1:
                        Console.WriteLine("Ingresar nombre");
                        nombre = Console.ReadLine();

                        Console.WriteLine("Ingresar numero de telefono");
                        telefono = int.Parse(Console.ReadLine());

                        c = new Contacto(nombre, telefono);

                        a.agregarContacto(c);

                        break;
                    case 2:
                        a.listaContactos();

                        break;
                    case 3:
                        Console.WriteLine("Ingresar nombre");
                        nombre = Console.ReadLine();

                        a.buscarPorNombre(nombre);

                        break;
                    case 4:
                        Console.WriteLine("Ingresar nombre");
                        nombre = Console.ReadLine();

                        c = new Contacto(nombre, 0);

                        if (a.existeContacto(c))
                            Console.WriteLine("El contacto si existe :D");
                        else
                            Console.WriteLine("El contacto no existe :(");

                        break;
                    case 5:
                        Console.WriteLine("Ingresar nombre del contacto a eliminar");
                        nombre = Console.ReadLine();

                        c = new Contacto(nombre, 0);
                        a.eliminarContacto(c);

                        break;
                    case 6:
                        Console.WriteLine("Hay " + a.huecoLibre() + " espacios de contactos disponibles");
                        break;
                    case 7:
                        if (a.agendaLlena())
                            Console.WriteLine("La agenda esta llena");
                        else
                            Console.WriteLine("La agenda todavia no esta llena");

                        break;
                    case 8:
                        salir = true;
                        break;
                    default:
                        Console.WriteLine("Ingrese un numero entre 1 y 8 por favor");
                        break;
                }
            }
        }
    }
}
