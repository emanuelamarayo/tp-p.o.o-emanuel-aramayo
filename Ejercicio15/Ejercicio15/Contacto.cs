﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio15
{
    class Contacto
    {
        private string nombre;
        private int nro;

        public Contacto(string nombre, int nro)
        {
            this.nombre = nombre;
            this.nro = nro;
        }
        public Contacto(string nombre)
        {
            this.nombre = nombre;
            this.nro = 0;
        }
        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                nombre = value;
            }
        }
        public int Nro
        {
            get
            {
                return nro;
            }
            set
            {
                nro = value;
            }
        }
        public bool equals(Contacto c)
        {
            if (this.nombre.Trim().Equals(c.Nombre.Trim()))
                return true;
            return false;
        }
    }
}
