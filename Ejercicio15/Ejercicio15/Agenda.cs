﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio15
{
    class Agenda
    {
        private Contacto[] contactos;

        public Agenda()
        {
            this.contactos = new Contacto[10];
        }
        public Agenda(int tamanio)
        {
            this.contactos = new Contacto[tamanio];
        }
        public void agregarContacto(Contacto c)
        {
            if (existeContacto(c))
                Console.WriteLine("El contacto con ese nombre ya existe");
            else if (agendaLlena())
                Console.WriteLine("La agenda esta llena");
            else
            {
                bool encontrado = false;
                for(int i = 0; i < contactos.Length && !encontrado; i++)
                {
                    if(contactos[i] == null)
                    {
                        contactos[i] = c;
                        encontrado = true;
                    }
                }
                if (encontrado)
                    Console.WriteLine("Se agrego correctamente");
                else
                {
                    Console.WriteLine("No se pudo agregar el contacto");
                }
            }
        }
        public bool existeContacto(Contacto c)
        {
            for(int i = 0; i < contactos.Length; i++)
            {
                if (contactos[i] != null && c.equals(contactos[i]))
                    return true;
            }
            return false;
        }
        public void listaContactos()
        {
            if (huecoLibre() == contactos.Length)
                Console.WriteLine("La agenda esta vacia");
            else
            {
                for(int i = 0; i < contactos.Length; i++)
                {
                    if (contactos[i] != null)
                        Console.WriteLine("Nombre: " + contactos[i].Nombre + " Telefono: " + contactos[i].Nro);
                }
            }
        }
        public void buscarPorNombre(string nombre)
        {
            bool encontrado = false;
            for(int i = 0; i < contactos.Length && !encontrado; i++)
            {
                if(contactos[i] != null && contactos[i].Nombre.Trim().Equals(nombre.Trim()))
                {
                    Console.WriteLine("El telefono: " + contactos[i].Nro + ", Su nombre: " + contactos[i].Nombre);
                    encontrado = true;
                }
            }
            if (!encontrado)
                Console.WriteLine("No se ha encontrado el contacto");
        }
        public bool agendaLlena()
        {
            for(int i = 0; i < contactos.Length; i++)
            {
                if (contactos[i] == null)
                    return false;
            }
            return true;
        }
        public int huecoLibre()
        {
            int contLibre = 0;
            for(int i = 0; i < contactos.Length; i++)
            {
                if (contactos[i] == null)
                    contLibre++;
            }
            return contLibre;
        }
        public void eliminarContacto(Contacto c)
        {
            bool encontrado = false;
            for(int i = 0; i < contactos.Length && !encontrado; i++)
            {
                if (contactos[i] != null && contactos[i].equals(c))
                {
                    contactos[i] = null;
                    encontrado = true;
                }
            }
            if (encontrado)
            {
                Console.WriteLine("Se elimino correctamente");
            }
            else
            {
                Console.WriteLine("No se elimino el contacto");
            }
        }
    }
}
