﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio12
{
    class Revolver
    {
        private int posicionBalaActual;
        private int posicionBala;

        public Revolver()
        {
            posicionBalaActual = MetodoRandom.Rdn(1, 6);
            posicionBala = MetodoRandom.Rdn(1, 6);
        }
        public bool Disparar()
        {
            bool exito = false;

            if (posicionBalaActual == posicionBala)
                exito = true;

            SiguienteBala();
            return exito;
        }

        public void SiguienteBala()
        {
            if (posicionBalaActual == 6)
                posicionBalaActual = 1;
            else
                posicionBalaActual++;
        }
        public string toString()
        {
            return "Posicion de la bala actual: " + posicionBalaActual + ", posicion de la bala: " + posicionBala;
        }
    }
}
