﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio12
{
    class Juego
    {
        private Jugador[] jugadores;
        private Revolver revolver;

        public Juego(int nroJugadores)
        {
            jugadores = new Jugador[ComprobarJugadores(nroJugadores)];

            CrearJugadores();

            revolver = new Revolver();
        }
        private int ComprobarJugadores(int numJugadores)
        {
            if (!(numJugadores >= 1 && numJugadores <= 6))
                numJugadores = 6;
            return numJugadores;
        }
        private void CrearJugadores()
        {
            for (int i = 0; i < jugadores.Length; i++)
            {
                jugadores[i] = new Jugador(i + 1);
            }
        }
        public bool FinJuego()
        {
            for(int i=0; i< jugadores.Length; i++)
            {
                if (!jugadores[i].isVivo())
                    return true;
            }
            return false;
        }
        public void Ronda()
        {
            for (int i = 0; i < jugadores.Length; i++)
            {
                jugadores[i].Disparar(revolver);
            }
        }
        public void Rondav2()
        {
            for(int i = 0; i < jugadores.Length; i++)
            {
                jugadores[i].Disparar(revolver);
                if (!jugadores[i].isVivo())
                    return;
            }
        }
    }
}
