﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio12
{
    class Jugador
    {
        private int id;
        private string nombre;
        private bool vivo;

        public Jugador(int id)
        {
            this.id = id;
            this.nombre = "Jugador " + id;
            this.vivo = true;
        }
        public void Disparar(Revolver r)
        {
            Console.WriteLine("El " + nombre + " se apunta con la pistola");

            if (r.Disparar())
            {
                this.vivo = false;
                Console.WriteLine("F por el " + nombre);
            }
            else
                Console.WriteLine("El " + nombre + " sobrevivio :D");
            Console.WriteLine("");
        }
        public bool isVivo()
        {
            return vivo;
        }
    }
}
