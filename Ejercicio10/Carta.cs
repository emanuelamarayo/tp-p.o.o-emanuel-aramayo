﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio10
{
    class Carta
    {
        public static string[] PALOS = new string[] { "Espadas", "Oros", "Copas", "Bastos" };
        public static int LIMITE_CARTAS_PALO = 12;

        private int nro;
        private string palo;

        public Carta(int nro, string palo)
        {
            this.nro = nro;
            this.palo = palo;
        }
        
        public int Nro
        {
            get
            {
                return nro;
            }
        }
        public string Palo
        {
            get
            {
                return palo;
            }
        }

    }
}
