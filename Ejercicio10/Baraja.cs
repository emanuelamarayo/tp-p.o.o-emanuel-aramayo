﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio10
{
    class Baraja
    {
        public static int NUM_CARTAS = 40;

        private Carta[] cartas;

        private int posSiguienteCarta;

        public Baraja()
        {
            this.cartas = new Carta[NUM_CARTAS];
            this.posSiguienteCarta = 0;
            CrearBaraja();
            Barajar();
        }

        private void CrearBaraja()
        {
            string[] palos = Carta.PALOS;
            for (int i = 0; i < palos.Length; i++)
            {
                for (int j = 0; j < Carta.LIMITE_CARTAS_PALO; j++)
                {
                    if (j != 7 || j != 8)
                    {
                        if (j >= 9)
                        {
                            cartas[((i * (Carta.LIMITE_CARTAS_PALO - 2)) + (j - 2))] = new Carta(j + 1, palos[i]);
                        }
                        else
                        {
                            cartas[((i * (Carta.LIMITE_CARTAS_PALO - 2)) + j)] = new Carta(j + 1, palos[i]);
                        }
                    }
                }
            }
        }

        private static int NroAleatorio(int min, int max)
        {
            Random rdn = new Random();
            return rdn.Next(min, max);
        }

        public void Barajar()
        {
            int posAleatoria = 0;

            Carta carta;

            for(int i = 0; i < cartas.Length; i++)
            {
                posAleatoria = NroAleatorio(0, NUM_CARTAS - 1);
                carta = cartas[i];
                cartas[i] = cartas[posAleatoria];
                cartas[posAleatoria] = carta;
            }
        }
        public Carta SiguienteCarta()
        {
            Carta carta = null;

            if (posSiguienteCarta == NUM_CARTAS)
                Console.WriteLine("No quedan cartas");
            else carta = cartas[posSiguienteCarta++];
            return carta;
        }

        public Carta[] DarCartas(int numCartas)
        {
            if (numCartas > NUM_CARTAS)
                Console.WriteLine("No se pueden dar mas cartas");
            else if (CartasDisponibles() < numCartas)
                Console.WriteLine("No alcanzan las cartas");
            else
            {
                Carta[] DarCartas = new Carta[numCartas];

                for(int i = 0;i < DarCartas.Length; i++)
                {
                    DarCartas[i] = SiguienteCarta();
                }
                return DarCartas;
            }
            return null;
        }

        public int CartasDisponibles()
        {
            return NUM_CARTAS - posSiguienteCarta;
        }
        public void CartasMonton()
        {
            if (CartasDisponibles() == NUM_CARTAS)
                Console.WriteLine("Todavia no se saco una carta");
            else
            {
                for(int i = 0; i < posSiguienteCarta; i++)
                {
                    Console.WriteLine(cartas[i].Nro + " de " + cartas[i].Palo);
                }
            }
        }
        public void MostrarBaraja()
        {
            if (CartasDisponibles() == 0)
                Console.WriteLine("No quedan cartas");
            else
            {
                for(int i = 0; i < posSiguienteCarta; i++)
                {
                    Console.WriteLine(cartas[i].Nro + " de " + cartas[i].Palo);
                }
            }
        }
    }
}