﻿using System;

namespace Ejercicio10
{
    class Program
    {
        static void Main(string[] args)
        {
            Baraja b = new Baraja();
            
            Console.WriteLine("Cartas disponibles: " + b.CartasDisponibles());

            b.SiguienteCarta();

            b.DarCartas(5);

            Console.WriteLine("Cartas disponibles: " + b.CartasDisponibles());

            b.CartasMonton();

            b.Barajar();

            Carta[] c = b.DarCartas(5);

            Console.WriteLine("Cartas sacadas después de barajar");

            for(int i = 0; i < c.Length; i++)
            {
                Console.WriteLine(c[i].Nro + " de " + c[i].Palo);
            }
        }
    }
}
