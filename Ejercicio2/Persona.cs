﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio2
{
    class Persona
    {
        private string nombre;
        private int edad;
        private int dni;
        private char sexo = 'H';
        private double peso;
        private double altura;

        private const int PESOIDEAL = -1;
        private const int MENOSPESO = 0;
        private const int SOBREPESO = 1;

        public Persona()
        {
            GenerarDni();
        }
        public Persona(string nombre, int edad, char sexo)
        {
            GenerarDni();
            this.nombre = nombre;
            this.edad = edad;
            this.sexo = sexo;
        }
        public Persona(string nombre, int edad, int dni, char sexo, double peso, double altura)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.dni = dni;
            this.sexo = sexo;
            this.peso = peso;
            this.altura = altura;
        }
        public int CalcularIMC()
        {
            double formula = peso / altura * 2;

            if (formula < 20)
            {
                return PESOIDEAL;
            }
            else if (formula >= 20 && formula <= 25)
            {
                return MENOSPESO;
            }
            else return SOBREPESO;
        }
        public bool EsMayorDeEdad()
        {
            if (edad >= 18)
            {
                return true;
            }
            else return false;
        }
        public void GenerarDni()
        {
            Random DniRandom = new Random();

            dni = DniRandom.Next(10000000, 99999999);
        }
        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                nombre = value;
            }
        }
        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                edad = value;
            }
        }
        public int Dni
        {
            get
            {
                return dni;
            }
        }
        public char Sexo
        {
            get
            {
                return sexo;
            }
            set
            {
                sexo = value;
            }
        }
        public double Peso
        {
            get
            {
                return peso;
            }
            set
            {
                peso = value;
            }
        }
        public double Altura
        {
            get
            {
                return altura;
            }
            set
            {
                altura = value;
            }
        }
    }
}
