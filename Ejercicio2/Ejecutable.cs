﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio2
{
    class Ejecutable
    {
        public void Ejecutar()
        {
            Console.WriteLine("Ingrese Nombre");
            string nombre = Console.ReadLine();
            Console.WriteLine("Ingrese Edad");
            int edad = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese DNI");
            int dni = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese Sexo con una letra");
            char sexo = char.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese Peso en KG");
            double peso = double.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese Altura en Metros");
            double altura = double.Parse(Console.ReadLine());

            Persona p1 = new Persona(nombre, edad, dni, sexo, peso, altura);
            Persona p2 = new Persona(nombre, edad, sexo);
            Persona p3 = new Persona();

            p3.Nombre = "Roy";
            p3.Edad = 13;
            p3.Sexo = 'M';
            p3.Peso = 60;
            p3.Altura = 1.7;

            static void PesoPersona(Persona p)
            {
                int datopeso = p.CalcularIMC();

                if (datopeso == -1)
                {
                    Console.WriteLine("Esta Persona tiene el peso ideal");
                }
                if (datopeso == 0)
                {
                    Console.WriteLine("Esta Persona no llega al peso ideal");
                }
                if (datopeso == 1)
                {
                    Console.WriteLine("Esta Persona tiene sobrepeso");
                }
            }

            static void MayordeEdad(Persona p)
            {
                bool datoedad = p.EsMayorDeEdad();

                if (datoedad == true)
                {
                    Console.WriteLine("Es mayor de edad");
                }
                else Console.WriteLine("No es mayor de edad");
            }

            PesoPersona(p1);
            PesoPersona(p2);
            PesoPersona(p3);

            MayordeEdad(p1);
            MayordeEdad(p2);
            MayordeEdad(p3);

            Console.WriteLine(p1.Nombre + " " + p1.Edad + " " + p1.Dni + " " + p1.Sexo + " " + p1.Peso + " " + p1.Altura);
            Console.WriteLine(p2.Nombre + " " + p2.Edad + " " + p2.Dni + " " + p2.Sexo + " " + p2.Peso + " " + p2.Altura);
            Console.WriteLine(p3.Nombre + " " + p3.Edad + " " + p3.Dni + " " + p3.Sexo + " " + p3.Peso + " " + p3.Altura);

        }
    }
}
