﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio12v2
{
    class Comercial : Empleado
    {
        private double comision;

        public Comercial(double comision, string nombre, int edad, double salario) : base(nombre,edad,salario)
        {
            this.comision = comision;
        }
        public double Comision
        {
            get
            {
                return comision;
            }
            set
            {
                comision = value;
            }
        }
        public override bool plus()
        {
            if(base.Edad < 30 && this.comision > 200)
            {
                double nuevoSalario = base.Salario + PLUS;
                base.Salario = nuevoSalario;
                Console.WriteLine("Se le agrego el plus al empleado " + base.Nombre);
                return true;
            }
            return false;
        }
    }
}
