﻿using System;

namespace Ejercicio12v2
{
    class Program
    {
        static void Main(string[] args)
        {
            Comercial c1 = new Comercial(300, "Ema", 21, 1000);
            Repartidor r1 = new Repartidor("zona 2", "PP", 23, 900);

            Console.WriteLine($"Nombre: {c1.Nombre} Edad: {c1.Edad} Salario: {c1.Salario} Comision: {c1.Comision}");
            Console.WriteLine($"Nombre: {r1.Nombre} Edad: {r1.Edad} Salario: {r1.Salario} Zona: {r1.Zona}");

            c1.plus();
            r1.plus();

            Console.WriteLine($"Nombre: {c1.Nombre} Edad: {c1.Edad} Salario: {c1.Salario} Comision: {c1.Comision}");
            Console.WriteLine($"Nombre: {r1.Nombre} Edad: {r1.Edad} Salario: {r1.Salario} Zona: {r1.Zona}");
        }
    }
}
