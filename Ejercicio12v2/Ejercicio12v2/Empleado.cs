﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio12v2
{
    abstract class Empleado
    {
        private string nombre;
        private int edad;
        private double salario;

        public static double PLUS = 300;

        public Empleado(string nombre, int edad, double salario)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.salario = salario;
        }
        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                nombre = value;
            }
        }
        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                edad = value;
            }
        }
        public double Salario
        {
            get
            {
                return salario;
            }
            set
            {
                salario = value;
            }
        }
        public abstract bool plus();
    }
}
