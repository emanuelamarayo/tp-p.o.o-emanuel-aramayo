﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio12v2
{
    class Repartidor : Empleado
    {
        private string zona;

        public Repartidor(string zona, string nombre, int edad, double salario) : base(nombre, edad, salario)
        {
            this.zona = zona;
        }

        public string Zona
        {
            get
            {
                return zona;
            }
            set
            {
                zona = value;
            }
        }
        public override bool plus()
        {
            if(base.Edad < 25 && this.zona.Contains("zona 3"))
            {
                double nuevoSalario = base.Salario + PLUS;
                base.Salario = nuevoSalario;
                Console.WriteLine("Se le agrego el plus al empleado " + base.Nombre);
                return true;
            }
            return false;
        }
    }
}
