﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio11
{
    class Jugador : Interfaz
    {
        private const int NUMERO_PARTIDOS = 2;
        private const double DINERO_INICIAL = 35;
        private const double DINERO_CADA_JORNADA = 1;
        private const int RESULTADO_MINIMO = 0;
        private const int RESULTADO_MAXIMO = 3;

        private string nombre;
        private double dinero;
        private int porrasGanadas;
        private string[] resultados;

        public Jugador(string nombre)
        {
            this.nombre = nombre;
            this.dinero = DINERO_INICIAL;
            this.porrasGanadas = 0;
            this.resultados = new string[NUMERO_PARTIDOS];
            ReiniciarResultados();
        }
        public void ReiniciarResultados()
        {
            for(int i=0; i < resultados.Length; i++)
            {
                resultados[i] = "";
            }
        }
        public bool PuedePagar()
        {
            return dinero >= DINERO_CADA_JORNADA;
        }
        public void PonerEnElBote()
        {
            dinero -= DINERO_CADA_JORNADA;
            Console.WriteLine("El jugador " + nombre + " ha puesto " + DINERO_CADA_JORNADA + " euro/s y le queda " + dinero);
        }
        public void GanarBote(double bote)
        {
            dinero += bote;
            porrasGanadas++;
            Console.WriteLine("El jugador " + nombre + " ha ganado " + bote + " euros/s y su dinero actual es de " + dinero);
        }
        public void GenerarResultados()
        {
            int pLocal;
            int pVisitante;
            
            for(int i = 0; i < resultados.Length; i++)
            {
                pLocal = MetodoRandom.Rdn(RESULTADO_MINIMO, RESULTADO_MAXIMO);
                pVisitante = MetodoRandom.Rdn(RESULTADO_MINIMO, RESULTADO_MAXIMO);

                resultados[i] = pLocal + " - " + pVisitante;

                Console.WriteLine("El jugador " + nombre + " ha elegido el resultado " + resultados[i]);
            }
            Console.WriteLine("");
        }
        public bool HaAcertadoPorra(string[] resultados_partidos)
        {
            for(int i = 0; i < resultados.Length; i++)
            {
                if (!resultados[i].Equals(resultados_partidos[i]))
                    return false;
            }
            return true;
        }
    }
}
