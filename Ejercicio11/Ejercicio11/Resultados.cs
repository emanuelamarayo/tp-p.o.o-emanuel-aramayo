﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio11
{
    class Resultados : Interfaz
    {
        private const int NUMERO_PARTIDOS = 2;
        private const int RESULTADO_MINIMO = 0;
        private const int RESULTADO_MAXIMO = 3;

        private string[] partidos;

        public Resultados()
        {
            partidos = new string[NUMERO_PARTIDOS];
        }
        public void GenerarResultados()
        {
            int pLocal;
            int pVisitante;

            for(int i = 0; i < partidos.Length; i++)
            {
                pLocal = MetodoRandom.Rdn(RESULTADO_MINIMO, RESULTADO_MAXIMO);
                pVisitante = MetodoRandom.Rdn(RESULTADO_MINIMO, RESULTADO_MAXIMO);

                partidos[i] = pLocal + " - " + pVisitante;

                Console.WriteLine($"El partido {i + 1} termino: {partidos[i]}");
            }
        }
        public string[] GetPartidos()
        {
            return partidos;
        }
    }
}
