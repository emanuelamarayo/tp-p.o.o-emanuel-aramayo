﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio11
{
    class ChisteMaloDePorra
    {
        private const double DINERO_CADA_JORNADA = 1;
        private const int NUMERO_JORNADAS = 38;
        private static Jugador[] JUGADORES =
        {
            new Jugador("Make"),
            new Jugador("JuanMa"),
            new Jugador("Fernando"),
            new Jugador("Alberto"),
            new Jugador("Lorente"),
            new Jugador("Adrian"),
            new Jugador("Maria"),
            new Jugador("Parra"),
            new Jugador("Pablo"),
            new Jugador("Prieto"),
            new Jugador("Ruben"),
            new Jugador("Jony"),
            new Jugador("Fran"),
            new Jugador("Isidoro"),
            new Jugador("Rafa")
        };
        private double bote;

        public ChisteMaloDePorra()
        {
            bote = 0;
        }
        public void AumentarBote(double dinero)
        {
            bote += dinero;
        }
        public void VaciarBote()
        {
            bote = 0;
        }
        public void Jornadas()
        {
            Resultados resultados = new Resultados();
            string[] partidos;

            for(int i=0; i < NUMERO_JORNADAS; i++)
            {
                Console.WriteLine($"Jornada: {i + 1}");
                Console.WriteLine("");

                for(int j= 0; j < JUGADORES.Length; j++)
                {
                    if (JUGADORES[j].PuedePagar())
                    {
                        JUGADORES[j].PonerEnElBote();
                        JUGADORES[j].GenerarResultados();
                        AumentarBote(DINERO_CADA_JORNADA);
                    }
                    else
                        JUGADORES[j].ReiniciarResultados();
                }

                resultados.GenerarResultados();
                partidos = resultados.GetPartidos();
                
                for(int j = 0; j < JUGADORES.Length; j++)
                {
                    if (JUGADORES[j].HaAcertadoPorra(partidos))
                    {
                        JUGADORES[j].GanarBote(bote);
                        VaciarBote();
                    }
                }
                Console.WriteLine("");
                Console.WriteLine("");

                Console.WriteLine("Bote en la porra: " + bote);

                Console.WriteLine("");
                Console.WriteLine("");
            }
        }
    }
}
